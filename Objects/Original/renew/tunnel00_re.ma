//Maya ASCII 2020 scene
//Name: tunnel00_re.ma
//Last modified: Thu, Apr 30, 2020 12:52:21 PM
//Codeset: 932
requires maya "2020";
requires "stereoCamera" "10.0";
requires "mtoa" "4.0.0";
currentUnit -l meter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "201911140446-42a737a01c";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18363)\n";
fileInfo "UUID" "E57BDCC2-4034-97DE-B4E7-42838334DF27";
createNode transform -s -n "persp";
	rename -uid "58A6C744-40ED-EBF3-46FC-2DAD4C6C4E3C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.0789349933867127 1.2667275988470035 0.52638299991671833 ;
	setAttr ".r" -type "double3" -12.338352729523837 -632.59999999994454 -1.7528357437945031e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "A1DB6FD1-4C5E-0191-E100-A8A20161AF66";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 4.6244819846616165;
	setAttr ".ow" 0.1;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -1.9073486328125e-06 50 1.71661376953125e-05 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "6B2FC6FF-4CD0-5925-016C-38B416F5C752";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.20000003500116692 10.016846376427303 0.50000005722045904 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "2AECE147-48D7-5B41-712E-6C8139F97780";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 9.1582823429190015;
	setAttr ".ow" 0.42105312005819306;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -20.000003500116691 85.856403350830078 50.000005722045898 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "361675BE-495B-FB26-2953-8A80987CF705";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.080942131703089365 0.503558322080083 10.041809745446868 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "E9032DE0-4079-0F79-2B1C-1CADE930F56F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 9.5418096882264081;
	setAttr ".ow" 2.2593680359364221;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" -20.000003500116691 85.856403350830078 50.000005722045898 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "38325E81-42B3-D153-FCA7-0D95BCE95388";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.03590460764117 0.39974279902644794 0.26684215676929091 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "7846C5A9-48B4-C027-9DAF-CE9BE883F18B";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 10.035904626714656;
	setAttr ".ow" 2.9571248892206605;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -1.9073486328125e-06 50 1.33514404296875e-05 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "tunnel00";
	rename -uid "A2ECE3F4-4D34-5AF4-EBEE-9A9E70E8D677";
createNode transform -n "ana_tunnel00" -p "tunnel00";
	rename -uid "1B532894-4715-4056-43D0-B5926FE02E5A";
	addAttr -is true -ci true -k true -sn "currentUVSet" -ln "currentUVSet" -dt "string";
	setAttr -k on ".currentUVSet" -type "string" "UVMap";
createNode mesh -n "ana_tunnel00Shape" -p "ana_tunnel00";
	rename -uid "375C5973-40C0-3F88-EDC2-9B8E7A1AE463";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1 1 ;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 180 ".uvst[0].uvsp[0:179]" -type "float2" 0.47683537 0.92993236
		 0.498474 0.8975606 0.74739754 0.20446432 0.40625036 0.95915818 1 1 0.33565462 0.92996001
		 1.000007390976 0.70313054 0.74739742 0.70313066 0.74739742 0.50133657 1.000007629395
		 0.50133657 0.30642855 0.85937476 0.74739742 0.34508955 0.36802638 0.95159888 0.74739742
		 0.29821575 0.47676861 0.92996001 1.000007629395 0.34508955 0.74739742 0.39196134
		 0.36805022 0.95156264 1.000007629395 0.29821575 0.74739337 0.90624893 1.00000333786
		 0.85937512 0.31402433 0.89757538 1.000007629395 0.39196146 0.4062115 0.95919728 0.49839866
		 0.89757538 0.74739194 0.95312655 1.000003099442 0.90625131 1.00000166893 0.95313132
		 0.31402671 0.89756083 0.30642807 0.85937572 0.44445074 0.95156264 0.44439662 0.95159888
		 0.74739754 0.25134581 0.74739337 0.85937512 1.000007629395 0.25134581 1.000004529953
		 0.81250328 0.74739456 0.81250393 0.33566558 0.92993259 0.25274232 0.97163177 0.028323382
		 0.74711829 0.25274232 0.74711829 0.028323382 0.97163129 0.25274232 0.97163129 0.25274256
		 0.74711829 0.028408259 0.74711829 0.25292686 0.74711829 0.028408259 0.74711829 0.028408259
		 0.97163129 0.028323382 0.74711829 0.25292686 0.74711829 0.25292662 0.97163129 0.25292686
		 0.97163177 0.028323382 0.97163177 0.028408259 0.97163129 0.30642736 0.74707484 0.51849926
		 0.74707341 0.29395425 0.74707341 0.30642736 0.74712014 0.51855123 0.74711895 0.5060724
		 0.85937524 0.50607216 0.74712014 0.51855123 0.971632 0.29394853 0.74711895 0.5060066
		 0.85937476 0.50607216 0.74707484 0.51846826 0.97167659 0.2939502 0.97163248 0.29395473
		 0.97167659 0.25734618 0.9841032 0.28148353 0.7424643 0.023714036 0.73464686 0.26520398
		 0.97623897 0.28934753 0.73459697 0.015846699 0.74251109 0.2654033 0.74251109 0.52316058
		 0.73464751 0.26520398 0.74251109 0.25753596 0.73464686 0.53102791 0.74251151 0.25734618
		 0.73464686 0.25734618 0.98410273 0.28147209 0.74251151 0.25753596 0.98410273 0.26520398
		 0.97623849 0.28933918 0.73464751 0.2654033 0.97623849 0.015846699 0.97623849 0.52307546
		 0.73459697 0.26520398 0.74251109 0.023714036 0.98410273 0.5309397 0.7424643 0.25734618
		 0.73464686 0.2654033 0.74251109 0.28934753 0.98415309 0.015946597 0.97623849 0.25753596
		 0.73464686 0.28148353 0.9762857 0.023804516 0.98410273 0.023804516 0.73464686 0.5309397
		 0.9762857 0.25753596 0.9841032 0.015946597 0.74251109 0.52307546 0.98415309 0.2654033
		 0.97623897 0.015846699 0.97623897 0.28933942 0.98410398 0.015946597 0.97623849 0.023714036
		 0.9841032 0.28147209 0.97623968 0.023804516 0.98410273 0.023804516 0.73464686 0.53102791
		 0.9762392 0.023714036 0.73464686 0.015946597 0.74251109 0.52316058 0.9841035 0.015846699
		 0.74251109 0.26520398 0.9841032 0.015846699 0.73464686 0.28148353 0.73459697 0.2654033
		 0.73464686 0.26520398 0.73464686 0.53102791 0.73464751 0.26520398 0.98410273 0.2654033
		 0.98410273 0.28147209 0.73464751 0.015846699 0.98410273 0.26520398 0.73464686 0.5309397
		 0.73459697 0.2654033 0.73464686 0.015946597 0.98410273 0.28148353 0.98415309 0.015946597
		 0.73464686 0.2654033 0.9841032 0.5309397 0.98415309 0.015846699 0.9841032 0.015946597
		 0.98410273 0.28147209 0.98410398 0.015946597 0.73464686 0.015846699 0.73464686 0.53102791
		 0.9841035 0.14057538 0.98410296 0.28148353 0.859375 0.14057538 0.97163129 0.29395449
		 0.859375 0.53102767 0.85937524 0.14057526 0.73464686 0.51855123 0.85937548 0.14057538
		 0.74711829 0.14057538 0.98410273 0.28147209 0.85937572 0.14057538 0.97163129 0.29394925
		 0.85937572 0.53093946 0.859375 0.14057526 0.73464686 0.51848376 0.859375 0.14057538
		 0.74711829 0.74739742 0.60223353 0.40624964 0.74711943 1.000007629395 0.60223341
		 0.4062382 0.74707413 0.4062115 0.73459697 0.015846699 0.85937482 0.028323382 0.85937482
		 0.2654033 0.85937482 0.40624988 0.73464727 0.25292662 0.85937482 0.2654033 0.85937506
		 0.4062115 0.98415309 0.25292686 0.85937506 0.4062115 0.97167659 0.015846699 0.85937506
		 0.40625012 0.9841035 0.028323382 0.85937482 0.40625083 0.97163224 0.30642879 0.95915818
		 0.5060724 0.95915818 0.30642927 0.95919776 0.50599349 0.95919728 0.74739027 0.99999428
		 1.000007629395 0.20446444;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 106 ".vt[0:105]"  0.40000004 0.050000008 -0.49999991 0.39999998 0.050000008 0.5
		 -0.40000015 0.050000008 0.5 -0.39999986 0.050000008 -0.5 -0.36955166 0.65307349 -0.49999991
		 -0.36955166 0.65307343 0.5 -0.39999986 0.5 -0.49999991 -0.39999986 0.5 0.49999991
		 0.40000015 0.5 -0.49999982 0.40000015 0.5 0.5000003 0.36955202 0.65307343 -0.49999976
		 0.36955202 0.65307343 0.50000036 0.28284287 0.78284287 -0.49999976 0.28284287 0.7828427 0.50000036
		 -0.15307301 0.86955202 -0.49999988 -0.15307301 0.8695519 0.50000024 8.8976506e-08 0.89999998 -0.49999982
		 8.6347903e-08 0.89999998 0.50000024 -0.28284246 0.78284287 -0.49999988 -0.28284246 0.78284287 0.5
		 0.15307352 0.8695519 -0.49999976 0.15307352 0.86955178 0.50000024 0.47539505 0.049994983 -0.47539496
		 0.5 0.049994983 -0.45000494 0.47539505 0.02460495 -0.45000497 0.45000502 0 -0.45000497
		 0.45000502 0.02460495 -0.47539496 0.45000502 0.049994983 -0.49999991 0.45000499 0.024604954 0.47539505
		 0.45000499 0 0.45000508 0.47539505 0.024604954 0.45000508 0.5 0.049994994 0.45000511
		 0.47539505 0.049994998 0.47539508 0.45000499 0.049994998 0.50000006 -0.47539517 0.049994975 0.47539502
		 -0.50000012 0.049994975 0.45000497 -0.47539517 0.024604954 0.45000497 -0.45000511 0 0.45000497
		 -0.45000514 0.02460495 0.47539496 -0.45000514 0.049994975 0.49999997 -0.45000482 0.024604958 -0.47539508
		 -0.45000482 0 -0.45000508 -0.4753949 0.024604958 -0.45000508 -0.49999985 0.049994983 -0.45000508
		 -0.4753949 0.049994994 -0.47539508 -0.45000482 0.049995001 -0.50000006 0.45000517 0.97539502 -0.47539476
		 0.45000517 1 -0.45000473 0.4753952 0.97539502 -0.45000473 0.50000018 0.95000505 -0.45000473
		 0.4753952 0.95000505 -0.47539479 0.45000517 0.95000505 -0.4999997 -0.47539505 0.95000505 -0.47539496
		 -0.5 0.95000505 -0.45000497 -0.47539508 0.97539502 -0.45000497 -0.45000502 1 -0.45000494
		 -0.45000499 0.97539502 -0.47539493 -0.45000499 0.95000505 -0.49999991 -0.45000523 0.97539502 0.47539493
		 -0.45000523 1 0.45000494 -0.47539529 0.97539502 0.45000488 -0.50000024 0.95000505 0.45000488
		 -0.47539529 0.95000505 0.47539493 -0.45000523 0.95000505 0.49999988 0.47539476 0.95000505 0.47539544
		 0.4999997 0.95000505 0.45000541 0.4753947 0.97539502 0.45000541 0.4500047 1 0.45000541
		 0.4500047 0.97539502 0.47539544 0.4500047 0.95000505 0.50000036 0.46729892 0.032701083 -0.46729887
		 0.46729892 0.032701086 0.46729895 -0.46729904 0.032701094 0.4672989 -0.46729875 0.032701086 -0.46729895
		 0.46729907 0.96729887 -0.46729863 -0.46729892 0.96729887 -0.46729887 -0.46729916 0.96729887 0.46729881
		 0.4672986 0.96729887 0.46729931 0.47539511 0.5 -0.4753949 0.50000006 0.5 -0.45000482
		 0.45000511 0.5 -0.49999982 0.4753949 0.49999997 0.47539523 0.45000485 0.5 0.50000024
		 0.49999985 0.49999997 0.45000526 -0.4753952 0.5 0.47539496 -0.50000018 0.5 0.45000494
		 -0.45000517 0.5 0.49999991 -0.47539496 0.49999997 -0.47539502 -0.45000488 0.5 -0.5
		 -0.49999991 0.5 -0.45000502 -2.8610229e-08 0.049997497 0.5 -2.8610229e-08 0.049997501 -0.5
		 0 0.024604954 -0.47539505 0 0 -0.45000502 -5.7220458e-08 0.02460495 0.47539496 -1.9073486e-08 0 0.45000502
		 5.7220458e-08 0.97539502 -0.47539482 -1.9073486e-08 1 -0.45000485 5.7220458e-08 0.95000505 -0.49999982
		 -3.8146972e-08 0.97539502 0.4753952 -3.8146972e-08 1 0.45000517 -3.8146972e-08 0.95000505 0.50000012
		 -0.39999986 0.89999998 0.5 0.40000015 0.89999998 0.5000003 0.40000015 0.89999998 -0.49999973
		 -0.39999986 0.89999998 -0.49999991;
	setAttr -s 238 ".ed";
	setAttr ".ed[0:165]"  3 2 0 2 90 0 1 0 0 0 91 0 0 8 0 9 1 0 2 7 0 7 5 0 5 19 0
		 19 15 0 15 17 0 17 21 0 21 13 0 13 11 0 11 9 0 8 10 0 10 12 0 12 20 0 20 16 0 14 18 0
		 18 4 0 4 6 0 10 11 1 13 12 1 8 9 1 14 15 1 19 18 1 20 21 1 17 16 1 5 4 1 23 22 1
		 22 78 1 50 49 1 49 79 1 22 27 1 27 80 1 51 50 1 25 24 1 24 30 1 30 29 1 29 25 1 24 23 1
		 23 31 1 31 30 1 27 26 1 26 92 1 40 45 1 26 25 1 25 93 1 41 40 1 29 28 1 28 94 1 38 37 1
		 37 95 1 28 33 1 1 28 1 39 38 1 33 32 1 32 81 1 64 69 1 69 82 1 32 31 1 31 83 1 65 64 1
		 35 34 1 34 84 1 62 61 1 61 85 1 34 39 1 39 86 1 63 62 1 37 36 1 36 42 1 42 41 1 41 37 1
		 36 35 1 35 43 1 43 42 1 45 44 1 44 87 1 52 57 1 57 88 1 44 43 1 43 89 1 53 52 1 47 46 1
		 46 96 1 56 55 1 55 97 1 46 51 1 51 98 1 57 56 1 49 48 1 48 66 1 66 65 1 65 49 1 48 47 1
		 47 67 1 67 66 1 55 54 1 54 60 1 60 59 1 59 55 1 54 53 1 53 61 1 61 60 1 59 58 1 58 99 1
		 68 67 1 67 100 1 58 63 1 63 101 1 69 68 1 2 39 1 33 1 1 22 70 1 70 26 1 24 70 1 28 71 1
		 71 32 1 30 71 1 34 72 1 72 38 1 36 72 1 40 73 1 73 44 1 42 73 1 46 74 1 74 50 1 48 74 1
		 52 75 1 75 56 1 54 75 1 58 76 1 76 62 1 60 76 1 64 77 1 77 68 1 66 77 1 104 80 1
		 27 8 1 9 33 1 39 7 1 88 105 1 6 3 0 7 6 1 78 50 1 79 23 1 80 51 1 81 64 1 82 33 1
		 83 65 1 84 62 1 85 35 1 86 63 1 87 52 1 88 45 1 89 53 1 82 103 1 102 86 1 6 45 1
		 78 79 1 80 78 1 81 82 1 83 81 1 84 85 1;
	setAttr ".ed[166:237]" 86 84 1 87 88 1 89 87 1 79 83 1 85 89 1 80 8 1 82 9 1
		 7 86 1 88 6 1 90 1 0 91 3 0 16 14 0 92 40 1 93 41 1 94 38 1 95 29 1 2 38 1 96 56 1
		 97 47 1 98 57 1 99 68 1 100 59 1 101 69 1 90 91 1 92 91 1 93 92 1 94 95 1 90 94 1
		 96 97 1 98 96 1 99 100 1 101 99 1 95 93 1 97 100 1 98 16 1 69 103 1 21 103 1 13 103 1
		 57 105 1 14 105 1 18 105 1 5 102 1 19 102 1 10 104 1 12 104 1 102 63 1 102 15 1 101 17 1
		 103 11 1 7 102 1 103 9 1 102 101 1 101 103 1 104 51 1 104 20 1 105 4 1 8 104 1 104 16 1
		 16 105 1 105 6 1 104 98 1 98 105 1 0 26 1 3 40 1 0 27 1 3 45 1 94 1 1 2 94 1 0 92 1
		 92 3 1 102 17 1 17 103 1;
	setAttr -s 132 -ch 476 ".fc[0:131]" -type "polyFaces" 
		f 4 0 1 189 176
		mu 0 4 6 7 156 158
		f 4 -3 -6 -25 -5
		mu 0 4 9 8 16 22
		f 4 22 -14 23 -17
		mu 0 4 15 11 13 18
		f 4 24 -15 -23 -16
		mu 0 4 22 16 11 15
		f 4 25 -10 26 -20
		mu 0 4 27 25 19 26
		f 4 27 -12 28 -19
		mu 0 4 34 32 2 179
		f 4 -27 -9 29 -21
		mu 0 4 26 19 33 20
		f 4 -24 -13 -28 -18
		mu 0 4 18 13 32 34
		f 4 -30 -8 145 -22
		mu 0 4 20 33 36 35
		f 4 -1 -145 -146 -7
		mu 0 4 7 6 35 36
		f 4 30 31 161 147
		mu 0 4 38 68 140 142
		f 4 34 35 162 -32
		mu 0 4 69 56 143 141
		f 4 37 38 39 40
		mu 0 4 39 70 77 49
		f 4 41 42 43 -39
		mu 0 4 71 38 40 76
		f 3 234 190 -4
		mu 0 3 54 160 159
		f 4 47 48 191 -46
		mu 0 4 73 39 162 161
		f 4 50 51 192 181
		mu 0 4 49 74 163 165
		f 3 193 232 -176
		mu 0 3 157 164 60
		f 3 -56 -233 -52
		mu 0 3 75 60 164
		f 4 57 58 163 150
		mu 0 4 58 78 144 146
		f 4 61 62 164 -59
		mu 0 4 79 40 147 145
		f 4 64 65 165 153
		mu 0 4 42 80 148 150
		f 4 68 69 166 -66
		mu 0 4 81 62 151 149
		f 4 71 72 73 74
		mu 0 4 50 82 89 41
		f 4 75 76 77 -73
		mu 0 4 83 42 43 88
		f 4 78 79 167 156
		mu 0 4 55 90 152 154
		f 4 82 83 168 -80
		mu 0 4 91 43 155 153
		f 4 85 86 194 184
		mu 0 4 45 92 166 168
		f 4 89 90 195 -87
		mu 0 4 93 67 169 167
		f 4 92 93 94 95
		mu 0 4 53 94 113 44
		f 4 96 97 98 -94
		mu 0 4 95 45 48 112
		f 4 99 100 101 102
		mu 0 4 51 100 107 52
		f 4 103 104 105 -101
		mu 0 4 101 46 47 106
		f 4 106 107 196 187
		mu 0 4 52 104 170 172
		f 4 110 111 197 -108
		mu 0 4 105 66 173 171
		f 4 -41 -182 198 -49
		mu 0 4 39 49 165 162
		f 4 -185 199 -110 -98
		mu 0 4 45 168 172 48
		f 4 -148 169 -63 -43
		mu 0 4 38 142 147 40
		f 4 -154 170 -84 -77
		mu 0 4 42 150 155 43
		f 3 -140 219 -149
		mu 0 3 143 176 67
		f 3 114 55 54
		mu 0 3 58 60 75
		f 3 -151 172 141
		mu 0 3 58 146 59
		f 3 -142 5 -115
		mu 0 3 58 59 60
		f 3 218 -202 -189
		mu 0 3 173 175 61
		f 3 -159 -61 201
		mu 0 3 175 146 61
		f 3 173 -70 142
		mu 0 3 29 151 62
		f 3 6 -143 -114
		mu 0 3 57 29 62
		f 3 -157 174 160
		mu 0 3 55 154 63
		f 3 227 -205 -186
		mu 0 3 169 177 65
		f 3 -144 -82 204
		mu 0 3 177 154 65
		f 4 -45 -35 115 116
		mu 0 4 72 56 69 118
		f 4 -31 -42 117 -116
		mu 0 4 68 38 71 116
		f 4 -38 -48 -117 -118
		mu 0 4 70 39 73 117
		f 4 -58 -55 118 119
		mu 0 4 78 58 75 121
		f 4 -51 -40 120 -119
		mu 0 4 74 49 77 119
		f 4 -44 -62 -120 -121
		mu 0 4 76 40 79 120
		f 4 -57 -69 121 122
		mu 0 4 84 62 81 124
		f 4 -65 -76 123 -122
		mu 0 4 80 42 83 122
		f 4 -72 -53 -123 -124
		mu 0 4 82 50 85 123
		f 4 -79 -47 124 125
		mu 0 4 90 55 87 127
		f 4 -50 -74 126 -125
		mu 0 4 86 41 89 125
		f 4 -78 -83 -126 -127
		mu 0 4 88 43 91 126
		f 4 -37 -90 127 128
		mu 0 4 96 67 93 130
		f 4 -86 -97 129 -128
		mu 0 4 92 45 95 128
		f 4 -93 -33 -129 -130
		mu 0 4 94 53 97 129
		f 4 -92 -81 130 131
		mu 0 4 102 65 99 133
		f 4 -85 -104 132 -131
		mu 0 4 98 46 101 131
		f 4 -100 -88 -132 -133
		mu 0 4 100 51 103 132
		f 4 -71 -111 133 134
		mu 0 4 108 66 105 136
		f 4 -107 -102 135 -134
		mu 0 4 104 52 107 134
		f 4 -106 -67 -135 -136
		mu 0 4 106 47 109 135
		f 4 -113 -60 136 137
		mu 0 4 114 61 111 139
		f 4 -64 -95 138 -137
		mu 0 4 110 44 113 137
		f 4 -99 -109 -138 -139
		mu 0 4 112 48 115 138
		f 4 -162 146 32 33
		mu 0 4 142 140 97 53
		f 4 -163 148 36 -147
		mu 0 4 141 143 67 96
		f 4 -164 149 59 60
		mu 0 4 146 144 111 61
		f 4 -165 151 63 -150
		mu 0 4 145 147 44 110
		f 4 -166 152 66 67
		mu 0 4 150 148 109 47
		f 4 -167 154 70 -153
		mu 0 4 149 151 66 108
		f 4 -168 155 80 81
		mu 0 4 154 152 99 65
		f 4 -169 157 84 -156
		mu 0 4 153 155 46 98
		f 4 -170 -34 -96 -152
		mu 0 4 147 142 53 44
		f 4 -171 -68 -105 -158
		mu 0 4 155 150 47 46
		f 3 -172 -36 140
		mu 0 3 10 143 56
		f 3 216 -173 158
		mu 0 3 175 59 146
		f 3 -155 -160 211
		mu 0 3 66 151 174
		f 3 225 -175 143
		mu 0 3 177 63 154
		f 4 -190 175 2 3
		mu 0 4 158 156 8 9
		f 4 -29 -11 -26 -178
		mu 0 4 4 178 25 27
		f 3 231 -161 144
		mu 0 3 64 55 63
		f 4 -192 179 49 -179
		mu 0 4 161 162 41 86
		f 4 -193 180 52 53
		mu 0 4 165 163 85 50
		f 3 -181 -234 182
		mu 0 3 84 164 57
		f 3 56 -183 113
		mu 0 3 62 84 57
		f 4 -195 183 87 88
		mu 0 4 168 166 103 51
		f 4 -196 185 91 -184
		mu 0 4 167 169 65 102
		f 4 -197 186 108 109
		mu 0 4 172 170 115 48
		f 4 -198 188 112 -187
		mu 0 4 171 173 61 114
		f 4 -199 -54 -75 -180
		mu 0 4 162 165 50 41
		f 4 -200 -89 -103 -188
		mu 0 4 172 168 51 52
		f 3 233 -194 -2
		mu 0 3 57 164 157
		f 3 217 -112 -212
		mu 0 3 174 173 66
		f 3 -174 215 159
		mu 0 3 151 29 174
		f 3 226 -91 -220
		mu 0 3 176 169 67
		f 3 171 222 139
		mu 0 3 143 10 176
		f 3 7 207 -216
		mu 0 3 29 28 174
		f 3 8 208 -208
		mu 0 3 28 37 174
		f 3 -209 9 -213
		mu 0 3 174 37 17
		f 3 12 203 -203
		mu 0 3 30 0 175
		f 3 13 -215 -204
		mu 0 3 0 1 175
		f 3 -217 214 14
		mu 0 3 59 175 1
		f 3 -218 236 -214
		mu 0 3 173 174 3
		f 3 -219 213 237
		mu 0 3 175 173 3
		f 3 15 209 -223
		mu 0 3 10 21 176
		f 3 16 210 -210
		mu 0 3 21 5 176
		f 3 -211 17 -221
		mu 0 3 176 5 12
		f 3 18 -224 220
		mu 0 3 12 23 176
		f 3 177 205 -225
		mu 0 3 23 31 177
		f 3 19 206 -206
		mu 0 3 31 14 177
		f 3 20 -222 -207
		mu 0 3 14 24 177
		f 3 -226 221 21
		mu 0 3 63 177 24
		f 3 223 -201 -227
		mu 0 3 176 23 169
		f 3 200 224 -228
		mu 0 3 169 23 177
		f 3 44 -229 230
		mu 0 3 56 72 54
		f 3 -177 -191 235
		mu 0 3 64 159 160
		f 3 -231 4 -141
		mu 0 3 56 54 10
		f 3 229 46 -232
		mu 0 3 64 87 55
		f 3 228 45 -235
		mu 0 3 54 72 160
		f 3 -236 178 -230
		mu 0 3 64 160 87
		f 3 -237 212 10
		mu 0 3 3 174 17
		f 3 -238 11 202
		mu 0 3 175 3 30;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 5 
		6 0 
		7 0 
		8 0 
		9 0 
		10 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode transform -n "Plate00";
	rename -uid "94FC8AF8-4EA6-AB37-B85F-B09C04717CFE";
	addAttr -is true -ci true -k true -sn "currentUVSet" -ln "currentUVSet" -dt "string";
	setAttr ".t" -type "double3" -1.4901161193847656e-08 1.25 0.50000003814697269 ;
	setAttr -k on ".currentUVSet" -type "string" "UVMap";
createNode mesh -n "Plate00Shape" -p "Plate00";
	rename -uid "86F2B862-4615-D514-94E6-6DAA9A623599";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0 0.99999994 0 1.25
		 1 1 1 1.25;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr -s 4 ".vt[0:3]"  -0.44999999 -0.10000038 0 0.45000005 -0.10000038 0
		 -0.44999999 0.10000038 0 0.45000005 0.10000038 0;
	setAttr -s 4 ".ed[0:3]"  0 1 0 1 3 0 3 2 0 2 0 0;
	setAttr -s 4 ".n[0:3]" -type "float3"  0 0 1 0 0 1 0 0 1 0 0 1;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 2 3 1;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "EA69F239-4FB0-665B-3123-D4B266AF18FD";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "56EE0A0A-49C0-D861-E4F0-2C8BADAD62E7";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "F48E965E-45B6-6BCB-66EE-3FAB07E7129A";
createNode displayLayerManager -n "layerManager";
	rename -uid "801508ED-4A4A-5234-9519-18998B005B05";
createNode displayLayer -n "defaultLayer";
	rename -uid "A9B13D38-46AC-22CD-639D-2C879D059529";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "BDF40328-4C02-DF26-628C-7E936A3491FD";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "FBDE1B55-44FA-2131-8198-3197EB012B50";
	setAttr ".g" yes;
createNode lambert -n "lambert2";
	rename -uid "30007EF8-4191-18E4-40ED-2182B3CF1E03";
createNode shadingEngine -n "Plate00SG";
	rename -uid "F702B055-4036-BF61-B519-F1A18C7A020C";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "369E5B35-4B5F-BE1B-BC73-E298FB204B7D";
createNode file -n "stage_uv_01_2";
	rename -uid "957222EB-4376-B38A-B747-FBA738E84BEB";
	setAttr ".ftn" -type "string" "D:/ITIProj/blackhole_unity/DESIGN/Objects//sourceimages/Brock0001.png";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "427E3954-4ECA-0F08-A1DE-9F942B811C90";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "D234CCA1-47C3-962D-F884-2FB237165703";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 782\n            -height 350\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1570\n            -height 744\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 782\n            -height 350\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 781\n            -height 350\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -selectionOrder \"display\" \n                -expandAttribute 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n"
		+ "                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 0\n                -outliner \"graphEditor1OutlineEd\" \n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n"
		+ "                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1570\\n    -height 744\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"wireframe\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1570\\n    -height 744\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 1 -size 12 -divisions 2 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "0C898B6E-4E30-2687-2FCA-2290AE537A0A";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 200 -ast 0 -aet 200 ";
	setAttr ".st" 6;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "3F3CE2FC-43B6-1B23-CE4C-B5A5268DDD7B";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -789.28568292231921 -294.04760736321703 ;
	setAttr ".tgi[0].vh" -type "double2" 790.47615906549015 292.85713122004603 ;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "Plate00SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "Plate00SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "stage_uv_01_2.oc" "lambert2.c";
connectAttr "lambert2.oc" "Plate00SG.ss";
connectAttr "Plate00Shape.iog" "Plate00SG.dsm" -na;
connectAttr "ana_tunnel00Shape.iog" "Plate00SG.dsm" -na;
connectAttr "Plate00SG.msg" "materialInfo1.sg";
connectAttr "lambert2.msg" "materialInfo1.m";
connectAttr "stage_uv_01_2.msg" "materialInfo1.t" -na;
connectAttr "place2dTexture1.o" "stage_uv_01_2.uv";
connectAttr "place2dTexture1.ofu" "stage_uv_01_2.ofu";
connectAttr "place2dTexture1.ofv" "stage_uv_01_2.ofv";
connectAttr "place2dTexture1.rf" "stage_uv_01_2.rf";
connectAttr "place2dTexture1.reu" "stage_uv_01_2.reu";
connectAttr "place2dTexture1.rev" "stage_uv_01_2.rev";
connectAttr "place2dTexture1.vt1" "stage_uv_01_2.vt1";
connectAttr "place2dTexture1.vt2" "stage_uv_01_2.vt2";
connectAttr "place2dTexture1.vt3" "stage_uv_01_2.vt3";
connectAttr "place2dTexture1.vc1" "stage_uv_01_2.vc1";
connectAttr "place2dTexture1.ofs" "stage_uv_01_2.fs";
connectAttr ":defaultColorMgtGlobals.cme" "stage_uv_01_2.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "stage_uv_01_2.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "stage_uv_01_2.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "stage_uv_01_2.ws";
connectAttr "Plate00SG.pa" ":renderPartition.st" -na;
connectAttr "lambert2.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "stage_uv_01_2.msg" ":defaultTextureList1.tx" -na;
// End of tunnel00_re.ma
