//Maya ASCII 2020 scene
//Name: ana_n_hole_111.ma
//Last modified: Thu, Apr 30, 2020 12:46:10 PM
//Codeset: 932
requires maya "2020";
requires "stereoCamera" "10.0";
requires "mtoa" "4.0.0";
currentUnit -l meter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2020";
fileInfo "version" "2020";
fileInfo "cutIdentifier" "201911140446-42a737a01c";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18363)\n";
fileInfo "UUID" "985E4F28-4899-A352-C207-2B99E082E8BE";
createNode transform -s -n "persp";
	rename -uid "1E6551DE-436B-7C4B-7AA5-069C80864849";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.6052733028748474 1.4589536690411122 0.11471387885647609 ;
	setAttr ".r" -type "double3" 331.46164726946864 812.19999999998424 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "F913E995-4D61-D4B9-5DC9-4A8F2E79EDF1";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 4.2106163589204018;
	setAttr ".ow" 0.1;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 46.768089294433594 -3.2319163680076599 -47.500255584716797 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -s -n "top";
	rename -uid "80807B9D-4F0B-B04B-DA26-D1812CBCE1BC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.44148035251893947 10.001000000000001 0.52590630130387395 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "5295245C-43EE-6D36-C499-8984627E3A12";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 10.500999961853029;
	setAttr ".ow" 0.58099375295707012;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 0 -49.999996185302734 1.9073486328125e-05 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "FE11C847-4E7C-7C1D-E200-C2B0DFAB74C4";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 -0.50000003814697269 10.013490951356854 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "7090B5F2-4F1E-C67A-2185-6DB4FD9D93DC";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 10.013491161165204;
	setAttr ".ow" 2.4459550666522265;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 0 -50.000003814697266 -2.09808349609375e-05 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "4CE158E4-4F1A-A176-B6BF-A6BE3903D2C1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 10.013490951618573 -0.95021790294451625 0.017431221700468562 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "A5D16F0A-4C31-F9EF-F447-1DB014F0A301";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".fd" 0.05;
	setAttr ".coi" 10.013490951618573;
	setAttr ".ow" 1.5744304651601757;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 0 -50.000003814697266 -2.09808349609375e-05 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "ana_n_hole_111";
	rename -uid "0038F0F7-4105-E028-3DEC-9999B49C1654";
	setAttr ".rp" -type "double3" 0 -0.5 0 ;
	setAttr ".sp" -type "double3" 0 -0.5 0 ;
createNode mesh -n "ana_n_hole_111Shape" -p "ana_n_hole_111";
	rename -uid "83C075FC-488C-33E3-E5B8-7B9E8C699D86";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.23624996095895767 0.67187513411045074 ;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2 ".pt";
	setAttr ".pt[32]" -type "float3" 0 -1.1920929e-09 -1.9073486e-08 ;
	setAttr ".db" yes;
	setAttr ".dmb" yes;
	setAttr ".bw" 3;
	setAttr ".ns" 2;
	setAttr ".dn" yes;
	setAttr ".dr" 1;
	setAttr ".difs" yes;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode mesh -n "polySurfaceShape1" -p "ana_n_hole_111";
	rename -uid "7D35B353-47AC-2BB4-F89F-60A085539EE0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:103]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.26562504470348358 0.50918138027191162 ;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 193 ".uvst[0].uvsp[0:192]" -type "float2" 0.20661509 0.16683829
		 0.18976998 0.19016463 0.1106708 0.033977389 0.11067116 0.21602261 0.09375 0.25000006
		 0.18749994 0.25000006 0.21547067 0.11058342 0.034529209 0.11058336 0.13932943 0.21602249
		 0.0625 0.24999999 0.21875 0.25000006 0.125 0.25000006 0.060230196 0.1901648 0.034529328
		 0.13941646 0.15625 0.25 0.21875 0.25 -2.9802322e-08 0.24999996 0.20661509 0.083161592
		 0.13932943 0.033977389 0.15625 0.25 0.25 0.24999999 0.03125 0.24999999 0.16658497
		 0.042887211 0.28125 0.24999999 0.21547079 0.13941646 0.083415329 0.04288733 0.060229957
		 0.059835136 0.03125 0.25 0.125 0.25000006 0.043385088 0.083161533 0.083415449 0.20711261
		 0.16658509 0.20711261 0.043384969 0.16683829 0.25 0.25000006 0.18977034 0.059834957
		 0.28125 0.24999999 0.18750006 0.25000006 0.09375 0.25 0.0625 0.25000006 0 0.25 0.31249994
		 0.24999999 0.3125 0.25000006 0.12500072 0.12499958 0.43406144 0.94497019 0.45915058
		 0.93218678 0.47906142 0.91227609 0.49184495 0.88718683 0.49624997 0.85937506 0.40625015
		 0.94937509 0.40625003 0.76937515 0.37843838 0.77377999 0.35334924 0.78656328 0.33343843
		 0.80647427 0.3206549 0.83156341 0.31625 0.8593753 0 0.48437497 0.03125 0.48437497
		 0.0625 0.48437503 0.09375 0.48437503 0.125 0.48437503 0.15625 0.48437503 0.1875 0.48437503
		 0.21875 0.48437497 0.25 0.48437503 0.28125 0.48437503 0.3125 0.48437503 0 0.48437497
		 0.03125 0.48437503 0.0625 0.48437503 0.09375 0.48437497 0.125 0.48437503 0.15625
		 0.48437503 0.1875 0.48437503 0.21875 0.48437503 0.25 0.48437503 0.28125 0.48437497
		 0.3125 0.48437503 0.49184519 0.83156353 0.47906166 0.80647439 0.45915082 0.78656352
		 0.43406156 0.77377999 0.32065502 0.88718659 0.33343855 0.91227573 0.35334936 0.93218666
		 0.3784385 0.94497019 0.30518928 0.75831425 0.51875114 0.74687374 0.50731063 0.75831437
		 0.51875114 0.97187632 0.50731075 0.96043581 0.29374877 0.9718762 0.3051894 0.96043569
		 0.29374877 0.74687374 0.28125003 0.74225628 0.28913131 0.734375 0.53125012 0.74225628
		 0.52336872 0.734375 0.52336872 0.98437506 0.53125012 0.97649378 0.28913131 0.98437506
		 0.28125003 0.97649378 0.28125003 0.734375 0.53125012 0.734375 0.53125012 0.98437506
		 0.28125003 0.98437506 0.25296947 0.97206801 0.2561717 0.98438513 0.025078177 0.98438537
		 0.028280407 0.97206801 0.26524913 0.97528017 0.26524913 0.98438513 0.016000986 0.97528017
		 0.016000986 0.98438537 0.028280638 0.74668229 0.25296971 0.74668229 0.26524916 0.74347156
		 0.016001046 0.74347156 0.028280579 0.97206801 0.016000926 0.97528017 0.016000926
		 0.74347156 0.028280459 0.74668229 0.025078356 0.98438513 0.016000926 0.98438513 0.25296953
		 0.74668229 0.25296965 0.97206801 0.25617188 0.98438513 0.26524919 0.97528017 0.26524916
		 0.74347156 0.26524919 0.98438513 0.028280519 0.97206801 0.016000926 0.97528017 0.016000926
		 0.74347132 0.028280519 0.74668229 0.025078297 0.98438513 0.016000926 0.98438513 0.25296959
		 0.74668229 0.25296959 0.97206801 0.25617182 0.98438513 0.26524919 0.97528017 0.26524919
		 0.74347132 0.26524919 0.98438513 0.028280407 0.97206801 0.016000926 0.97528017 0.016000986
		 0.74347156 0.028280407 0.74668229 0.025078177 0.98438513 0.016000926 0.98438513 0.25296953
		 0.74668229 0.25296953 0.97206801 0.25617176 0.98438513 0.26524919 0.97528017 0.26524913
		 0.74347156 0.26524919 0.98438513 0.025078416 0.73436511 0.25617197 0.73436463 0.28125003
		 0.74225628 0.29374877 0.74687374 0.29374877 0.9718762 0.28125003 0.97649378 0.28913131
		 0.734375 0.52336872 0.734375 0.51875114 0.74687374 0.25617176 0.73436487 0.025078177
		 0.73436511 0.53125012 0.74225628 0.53125012 0.97649378 0.51875114 0.97187632 0.25617176
		 0.73436487 0.025078237 0.73436511 0.52336872 0.98437506 0.28913131 0.98437506 0.25617182
		 0.73436511 0.025078297 0.73436511 0.28125003 0.734375 0.016000986 0.74347013 0.016000986
		 0.73436511 0.26524913 0.74346983 0.26524913 0.73436487 0.016000807 0.74347013 0.016000807
		 0.73436511 0.26524919 0.74346983 0.26524919 0.73436487 0.53125012 0.734375 0.016000926
		 0.74347013 0.016000926 0.73436511 0.26524907 0.74347013 0.26524907 0.73436511 0.53125012
		 0.98437506 0.28125003 0.98437506 0.016000867 0.74347013 0.016000867 0.73436511 0.26524916
		 0.74347013 0.26524916 0.73436463;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 101 ".vt[0:100]"  8.4519385e-08 -0.95000494 -0.3600001 0.11124621 -0.95000494 -0.34238043
		 0.21160278 -0.95000494 -0.29124618 0.29124624 -0.95000494 -0.21160275 0.34238043 -0.95000494 -0.11124621
		 0.3600001 -0.95000494 -5.2847909e-08 0.34238043 -0.95000494 0.11124604 0.29124624 -0.95000494 0.21160263
		 0.21160278 -0.95000494 0.29124612 0.11124614 -0.95000494 0.34238029 -3.2783504e-08 -0.95000494 0.35999992
		 -0.11124618 -0.95000494 0.34238023 -0.21160278 -0.95000494 0.29124597 -0.29124618 -0.95000494 0.21160242
		 -0.34238034 -0.95000494 0.11124574 -0.35999984 -0.95000494 -4.161997e-07 -0.34238014 -0.95000494 -0.11124658
		 -0.29124579 -0.95000494 -0.21160311 -0.21160221 -0.95000494 -0.29124644 -0.11124553 -0.95000494 -0.34238061
		 2.5867941e-08 -0.95000494 -9.5367433e-08 0.11124621 -3.8146972e-08 -0.34238064 0.21160278 -3.8146972e-08 -0.29124638
		 0.29124624 -3.8146972e-08 -0.21160296 0.34238043 0 -0.11124641 0.3600001 0 -2.6272582e-07
		 8.4519385e-08 -7.6293944e-08 -0.36000025 -3.2783504e-08 7.6293944e-08 0.35999975
		 -0.11124618 3.8146972e-08 0.34238002 -0.21160278 3.8146972e-08 0.29124576 -0.29124618 3.8146972e-08 0.21160223
		 -0.34238034 0 0.11124554 -0.35999984 0 -6.2607762e-07 0.34238043 0 0.11124583 0.29124624 3.8146972e-08 0.21160242
		 0.21160278 3.8146972e-08 0.29124591 0.11124614 3.8146972e-08 0.34238011 -0.34238014 0 -0.11124679
		 -0.29124579 -3.8146972e-08 -0.21160331 -0.21160221 -3.8146972e-08 -0.29124665 -0.11124553 -3.8146972e-08 -0.34238079
		 -0.40424234 -6.1565601e-08 -0.4042429 0.40424266 -6.1565601e-08 -0.40424275 0.40424266 6.1565601e-08 0.40424243
		 -0.40424263 6.1565601e-08 0.40424237 -0.47626945 -0.023730468 0.44999105 -0.5 -0.049994923 0.45000497
		 -0.47626945 -0.050008811 0.47626936 -0.45000499 -0.049994912 0.49999997 -0.44999111 -0.023730468 0.47626936
		 -0.45000499 6.862161e-08 0.45000494 0.47626945 -0.050008811 0.47626936 0.5 -0.049994923 0.45000497
		 0.47626945 -0.023730468 0.44999105 0.45000499 6.862161e-08 0.45000494 0.44999111 -0.023730468 0.47626936
		 0.45000499 -0.049994912 0.49999997 0.47626945 -0.050008964 -0.47626954 0.45000499 -0.049995065 -0.50000006
		 0.44999111 -0.023730621 -0.47626954 0.45000499 -6.8621596e-08 -0.45000508 0.47626945 -0.023730621 -0.4499912
		 0.5 -0.049995061 -0.45000508 -0.44999111 -0.023730621 -0.47626954 -0.45000499 -0.049995076 -0.50000006
		 -0.47626945 -0.050008964 -0.47626954 -0.5 -0.049995061 -0.45000508 -0.47626945 -0.023730621 -0.4499912
		 -0.45000499 -6.8621581e-08 -0.45000508 -0.45000499 -0.95000494 0.5000003 -0.47626945 -0.94999802 0.47626945
		 -0.5 -0.95000494 0.45000526 -0.5 -0.95000505 -0.45000473 -0.47626945 -0.94999802 -0.47626945
		 -0.45000499 -0.95000505 -0.49999976 0.45000499 -0.95000505 -0.49999976 0.47626945 -0.94999802 -0.47626945
		 0.5 -0.95000505 -0.45000473 0.5 -0.95000494 0.45000526 0.47626945 -0.94999802 0.47626945
		 0.45000499 -0.95000494 0.5000003 -0.46810114 -0.031898767 0.46810108 0.46810114 -0.031898767 0.46810108
		 0.46810114 -0.031898919 -0.46810123 -0.46810114 -0.031898919 -0.46810123 -0.47626945 -0.97626954 -0.44999105
		 -0.44999111 -0.97626954 -0.47626936 -0.45000499 -1.000000119209 -0.45000494 0.47626945 -0.97626954 -0.44999105
		 0.45000499 -1.000000119209 -0.45000494 0.44999111 -0.97626954 -0.47626936 0.44999111 -0.97626936 0.47626954
		 0.45000499 -0.99999994 0.45000508 0.47626945 -0.97626936 0.4499912 -0.44999111 -0.97626936 0.47626954
		 -0.47626945 -0.97626936 0.4499912 -0.45000499 -0.99999994 0.45000508 -0.46810114 -0.9681012 -0.46810108
		 0.46810114 -0.9681012 -0.46810108 0.46810114 -0.96810102 0.46810123 -0.46810114 -0.96810102 0.46810123;
	setAttr -s 204 ".ed";
	setAttr ".ed[0:165]"  24 25 0 26 21 0 21 22 0 22 42 1 23 24 0 22 23 0 27 28 0
		 31 32 0 28 29 0 29 44 1 30 31 0 29 30 0 0 26 1 21 1 1 1 0 0 22 2 1 2 1 0 23 3 1 3 2 0
		 24 4 1 4 3 0 25 5 1 5 4 0 25 33 0 33 6 1 6 5 0 33 34 0 34 7 1 7 6 0 34 35 0 35 8 1
		 8 7 0 35 36 0 36 9 1 9 8 0 36 27 0 27 10 1 10 9 0 28 11 1 11 10 0 29 12 1 12 11 0
		 30 13 1 13 12 0 31 14 1 14 13 0 32 15 1 15 14 0 32 37 0 37 16 1 16 15 0 37 38 0 38 17 1
		 17 16 0 38 39 0 39 18 1 18 17 0 39 40 0 40 19 1 19 18 0 40 26 0 0 19 0 0 20 1 20 19 1
		 16 20 1 20 15 1 17 20 1 18 20 1 34 43 1 38 41 1 1 20 1 2 20 1 3 20 1 4 20 1 5 20 1
		 6 20 1 7 20 1 8 20 1 9 20 1 10 20 1 20 14 1 11 20 1 12 20 1 13 20 1 41 39 1 42 23 1
		 43 35 1 44 30 1 42 43 1 43 44 1 44 41 1 41 42 1 69 80 1 72 71 1 75 74 1 78 77 1 46 45 1
		 45 67 1 67 66 1 66 46 1 45 50 1 50 68 1 68 67 1 48 47 1 47 70 1 70 69 1 69 48 1 47 46 1
		 46 71 1 71 70 1 50 49 1 49 55 1 55 54 1 54 50 1 49 48 1 48 56 1 56 55 1 52 51 1 51 79 1
		 79 78 1 78 52 1 51 56 1 56 80 1 80 79 1 54 53 1 53 61 1 61 60 1 60 54 1 53 52 1 52 62 1
		 62 61 1 58 57 1 57 76 1 76 75 1 75 58 1 57 62 1 62 77 1 77 76 1 60 59 1 59 63 1 63 68 1
		 68 60 1 59 58 1 58 64 1 64 63 1 66 65 1 65 73 1 73 72 1 72 66 1 65 64 1 64 74 1 74 73 1
		 44 50 1 54 43 1 60 42 1 68 41 1 45 81 1 81 49 1 47 81 1 51 82 1 82 55 1 53 82 1 57 83 1
		 83 61 1 59 83 1 63 84 1;
	setAttr ".ed[166:203]" 84 67 1 65 84 1 72 85 1 85 95 1 95 71 1 85 87 1 87 96 0
		 96 95 1 87 86 1 86 90 1 90 89 1 89 87 0 86 74 1 75 90 1 89 88 1 88 93 1 93 92 1 92 89 0
		 88 77 1 78 93 1 92 91 1 91 94 1 94 96 1 96 92 0 91 80 1 69 94 1 85 97 1 97 86 1 73 97 1
		 76 98 1 98 90 1 88 98 1 79 99 1 99 93 1 91 99 1 94 100 1 100 95 1 70 100 1;
	setAttr -s 141 ".n[0:140]" -type "float3"  1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20
		 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20 1e+20;
	setAttr -s 104 -ch 404 ".fc[0:103]" -type "polyFaces" 
		f 4 -15 -14 -2 -13
		mu 0 4 39 27 56 55
		f 4 -17 -16 -3 13
		mu 0 4 27 38 57 56
		f 4 -19 -18 -6 15
		mu 0 4 38 4 58 57
		f 4 -21 -20 -5 17
		mu 0 4 4 11 59 58
		f 4 -23 -22 -1 19
		mu 0 4 11 19 60 59
		f 4 -26 -25 -24 21
		mu 0 4 19 36 61 60
		f 4 -29 -28 -27 24
		mu 0 4 36 15 62 61
		f 4 -32 -31 -30 27
		mu 0 4 15 20 63 62
		f 4 -35 -34 -33 30
		mu 0 4 20 23 64 63
		f 4 -38 -37 -36 33
		mu 0 4 23 40 65 64
		f 4 -40 -39 -7 36
		mu 0 4 16 21 67 66
		f 4 -42 -41 -9 38
		mu 0 4 21 9 68 67
		f 4 -44 -43 -12 40
		mu 0 4 9 37 69 68
		f 4 -46 -45 -11 42
		mu 0 4 37 28 70 69
		f 4 -48 -47 -8 44
		mu 0 4 28 14 71 70
		f 4 -51 -50 -49 46
		mu 0 4 14 5 72 71
		f 4 -54 -53 -52 49
		mu 0 4 5 10 73 72
		f 4 -57 -56 -55 52
		mu 0 4 10 33 74 73
		f 4 -60 -59 -58 55
		mu 0 4 33 35 75 74
		f 4 -62 12 -61 58
		mu 0 4 35 41 76 75
		f 3 61 -64 -63
		mu 0 3 24 0 42
		f 3 -66 -65 50
		mu 0 3 3 42 8
		f 3 64 -67 53
		mu 0 3 8 42 31
		f 3 66 -68 56
		mu 0 3 31 42 1
		f 3 63 59 67
		mu 0 3 42 0 1
		f 3 62 -71 14
		mu 0 3 24 42 6
		f 3 70 -72 16
		mu 0 3 6 42 17
		f 3 71 -73 18
		mu 0 3 17 42 34
		f 3 72 -74 20
		mu 0 3 34 42 22
		f 3 73 -75 22
		mu 0 3 22 42 18
		f 3 74 -76 25
		mu 0 3 18 42 2
		f 3 75 -77 28
		mu 0 3 2 42 25
		f 3 76 -78 31
		mu 0 3 25 42 26
		f 3 77 -79 34
		mu 0 3 26 42 29
		f 3 -80 37 78
		mu 0 3 42 7 29
		f 3 47 -81 65
		mu 0 3 3 30 42
		f 3 79 -82 39
		mu 0 3 7 42 13
		f 3 81 -83 41
		mu 0 3 13 42 32
		f 3 82 -84 43
		mu 0 3 32 42 12
		f 3 80 45 83
		mu 0 3 42 30 12
		f 3 -70 54 -85
		mu 0 3 91 82 83
		f 3 5 -86 -4
		mu 0 3 44 45 89
		f 7 23 26 68 -89 85 4 0
		mu 0 7 47 77 78 87 89 45 46
		f 3 29 -87 -69
		mu 0 3 78 79 87
		f 7 35 6 8 9 -90 86 32
		mu 0 7 80 49 50 51 85 87 79
		f 3 -10 11 -88
		mu 0 3 85 51 52
		f 7 48 51 69 -91 87 10 7
		mu 0 7 54 81 82 91 85 52 53
		f 7 60 1 2 3 -92 84 57
		mu 0 7 84 48 43 44 89 91 83
		f 4 96 97 98 99
		mu 0 4 105 106 107 108
		f 4 100 101 102 -98
		mu 0 4 93 92 90 100
		f 4 103 104 105 106
		mu 0 4 129 130 131 132
		f 4 107 108 109 -105
		mu 0 4 109 105 114 115
		f 4 110 111 112 113
		mu 0 4 92 94 96 86
		f 4 114 115 116 -112
		mu 0 4 133 129 136 137
		f 4 117 118 119 120
		mu 0 4 117 118 119 120
		f 4 121 122 123 -119
		mu 0 4 138 136 135 139
		f 4 124 125 126 127
		mu 0 4 86 95 98 88
		f 4 128 129 130 -126
		mu 0 4 121 117 124 125
		f 4 131 132 133 134
		mu 0 4 141 142 143 144
		f 4 135 136 137 -133
		mu 0 4 126 124 123 127
		f 4 138 139 140 141
		mu 0 4 88 97 99 90
		f 4 142 143 144 -140
		mu 0 4 145 141 148 149
		f 4 145 146 147 148
		mu 0 4 108 111 116 113
		f 4 149 150 151 -147
		mu 0 4 150 148 147 151
		f 4 89 152 -114 153
		mu 0 4 87 85 92 86
		f 4 88 -154 -128 154
		mu 0 4 89 87 86 88
		f 4 91 -155 -142 155
		mu 0 4 91 89 88 90
		f 4 90 -156 -102 -153
		mu 0 4 85 91 90 92
		f 4 -149 93 -109 -100
		mu 0 4 108 113 114 105
		f 4 -135 94 -151 -144
		mu 0 4 141 144 147 148
		f 4 -121 95 -137 -130
		mu 0 4 117 120 123 124
		f 4 -107 92 -123 -116
		mu 0 4 129 132 135 136
		f 4 -111 -101 156 157
		mu 0 4 94 92 93 101
		f 4 -97 -108 158 -157
		mu 0 4 106 105 109 110
		f 4 -104 -115 -158 -159
		mu 0 4 130 129 133 134
		f 4 -117 -122 159 160
		mu 0 4 137 136 138 140
		f 4 -118 -129 161 -160
		mu 0 4 118 117 121 122
		f 4 -125 -113 -161 -162
		mu 0 4 95 86 96 102
		f 4 -131 -136 162 163
		mu 0 4 125 124 126 128
		f 4 -132 -143 164 -163
		mu 0 4 142 141 145 146
		f 4 -139 -127 -164 -165
		mu 0 4 97 88 98 103
		f 4 -103 -141 165 166
		mu 0 4 100 90 99 104
		f 4 -145 -150 167 -166
		mu 0 4 149 148 150 152
		f 4 -146 -99 -167 -168
		mu 0 4 111 108 107 112
		f 4 168 169 170 -94
		mu 0 4 113 153 154 114
		f 4 171 172 173 -170
		mu 0 4 155 156 157 158
		f 4 174 175 176 177
		mu 0 4 156 159 160 161
		f 4 178 -95 179 -176
		mu 0 4 162 147 144 163
		f 4 180 181 182 183
		mu 0 4 161 164 165 166
		f 4 184 -96 185 -182
		mu 0 4 167 123 120 168
		f 4 186 187 188 189
		mu 0 4 166 169 170 157
		f 4 190 -93 191 -188
		mu 0 4 171 135 132 172
		f 4 -175 -172 192 193
		mu 0 4 159 156 155 173
		f 4 -169 -148 194 -193
		mu 0 4 153 113 174 175
		f 4 -152 -179 -194 -195
		mu 0 4 176 147 162 177
		f 4 -180 -134 195 196
		mu 0 4 163 144 178 179
		f 4 -138 -185 197 -196
		mu 0 4 180 123 167 181
		f 4 -181 -177 -197 -198
		mu 0 4 164 161 160 182
		f 4 -186 -120 198 199
		mu 0 4 168 120 183 184
		f 4 -124 -191 200 -199
		mu 0 4 185 135 171 186
		f 4 -187 -183 -200 -201
		mu 0 4 169 166 165 187
		f 4 -174 -189 201 202
		mu 0 4 158 157 170 188
		f 4 -192 -106 203 -202
		mu 0 4 172 132 189 190
		f 4 -110 -171 -203 -204
		mu 0 4 191 114 154 192;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 8 
		113 0 
		114 0 
		120 0 
		123 0 
		132 0 
		135 0 
		144 0 
		147 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".db" yes;
	setAttr ".dmb" yes;
	setAttr ".bw" 3;
	setAttr ".ns" 2;
	setAttr ".dn" yes;
	setAttr ".difs" yes;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A6022582-4EBD-405A-8A4E-0585BADA81B9";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "6C34CC2C-4F99-E85C-1059-E19972DDD415";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "7D7F9BCF-4837-3F64-FD35-38B409AF92C0";
createNode displayLayerManager -n "layerManager";
	rename -uid "4B580735-4D03-733C-CC8F-248EDF4DB583";
createNode displayLayer -n "defaultLayer";
	rename -uid "CB064CFC-4C17-10E3-D8CD-EBAC30C74FAB";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "C6D68253-4E08-F81D-5190-8B83996980AB";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "ECB40358-4960-3875-BDA5-8D908F0CA403";
	setAttr ".g" yes;
createNode lambert -n "lambert3";
	rename -uid "AEE3C70D-48DE-8336-9D72-798A2E36FACC";
createNode shadingEngine -n "ana_n_hole_111SG";
	rename -uid "9E6B2CCE-4291-7997-39B5-53A1D55A1F42";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "CA620986-4F6F-1D29-4A95-9E917F22C30B";
createNode file -n "stage_uv_01_3";
	rename -uid "97044862-4147-F57C-DD03-CFBB814D0FEC";
	setAttr ".ftn" -type "string" "D:/ITIProj/blackhole_unity/DESIGN/Objects//sourceimages/Brock0001.png";
	setAttr ".cs" -type "string" "sRGB";
createNode place2dTexture -n "place2dTexture1";
	rename -uid "DAA1FB4C-42AA-34FA-8DA5-07971A612328";
createNode script -n "ana_n_wall_new:uiConfigurationScriptNode";
	rename -uid "EA33DE69-406C-D414-0409-B6AC5260EB38";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 782\n            -height 350\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 781\n            -height 350\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 782\n            -height 350\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n"
		+ "            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1570\n            -height 744\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n"
		+ "                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -selectionOrder \"display\" \n                -expandAttribute 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n"
		+ "                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 0\n                -outliner \"graphEditor1OutlineEd\" \n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n"
		+ "                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n"
		+ "                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n"
		+ "                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n"
		+ "                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n"
		+ "                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n"
		+ "                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n"
		+ "                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1570\\n    -height 744\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1570\\n    -height 744\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 1 -size 12 -divisions 2 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "ana_n_wall_new:sceneConfigurationScriptNode";
	rename -uid "67182A48-4EF8-742F-18F2-19BE4EB77219";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 120 -ast 0 -aet 200 ";
	setAttr ".st" 6;
createNode polyCut -n "polyCut1";
	rename -uid "0059DB89-43D4-24FA-2282-9EACAD51B6E4";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 -0.5 1.1444091796875e-07 ;
	setAttr ".ps" -type "double2" 1 1.0000001525878905 ;
createNode groupId -n "groupId1";
	rename -uid "D7D1462E-4065-EA76-85C0-A89728DF2536";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "06F59825-40A9-E01A-1922-84A2EB2103D4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:103]";
createNode polyCut -n "polyCut2";
	rename -uid "64874CFC-43D8-294F-26BE-B5BBCF48C2AF";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pc" -type "double3" 0 -0.5 1.1444091796875e-07 ;
	setAttr ".ro" -type "double3" 0 90 0 ;
	setAttr ".ps" -type "double2" 1.0000003814697265 1.0000001525878905 ;
createNode polyMergeVert -n "polyMergeVert1";
	rename -uid "2A3C665B-4E00-DF7A-E9C9-338C0198D96B";
	setAttr ".ics" -type "componentList" 1 "vtx[0:156]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 0.001;
	setAttr ".am" yes;
createNode polySplit -n "polySplit1";
	rename -uid "EEE11963-49CB-6417-075E-188F5D6B000A";
	setAttr -s 16 ".e[0:15]"  1 0 1 0 1 0 0 0 1 0 1 0 1 0 1 0;
	setAttr -s 16 ".d[0:15]"  -2147483638 -2147483565 -2147483642 -2147483415 -2147483617 -2147483566 
		-2147483623 -2147483448 -2147483644 -2147483567 -2147483647 -2147483414 -2147483594 -2147483564 -2147483603 -2147483447;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit2";
	rename -uid "B4BFF789-4DF5-B3C7-FD94-E2AAE1528920";
	setAttr -s 2 ".e[0:1]"  0 0;
	setAttr -s 2 ".d[0:1]"  -2147483432 -2147483641;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polySplit -n "polySplit3";
	rename -uid "DF0A79AE-4CFD-C52A-2DF5-FD830E4E7765";
	setAttr -s 11 ".e[0:10]"  0 1 1 1 1 1 1 0.91483498 0.79157501 0.076991603
		 1;
	setAttr -s 11 ".d[0:10]"  -2147483503 -2147483554 -2147483447 -2147483514 -2147483414 -2147483528 
		-2147483448 -2147483542 -2147483399 -2147483410 -2147483415;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyDelEdge -n "polyDelEdge1";
	rename -uid "67B9BF70-4FDF-397B-5873-09B89851C89B";
	setAttr ".ics" -type "componentList" 1 "e[286:289]";
	setAttr ".cv" yes;
createNode polySplit -n "polySplit4";
	rename -uid "465EFE8A-4814-DF2E-A84D-259A96B66612";
	setAttr -s 3 ".e[0:2]"  1 0 1;
	setAttr -s 3 ".d[0:2]"  -2147483416 -2147483411 -2147483503;
	setAttr ".sma" 180;
	setAttr ".m2015" yes;
createNode polyMergeVert -n "polyMergeVert2";
	rename -uid "46F7C546-4B20-EC54-6AFB-C8BCB2497ACD";
	setAttr ".ics" -type "componentList" 1 "vtx[0:129]";
	setAttr ".ix" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".d" 0.001;
	setAttr ".am" yes;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "972304FB-4C09-1C24-1772-CF9259BF5090";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -789.28568292231921 -294.04760736321703 ;
	setAttr ".tgi[0].vh" -type "double2" 790.47615906549015 292.85713122004603 ;
select -ne :time1;
	setAttr ".o" 0;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr ".ren" -type "string" "arnold";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyMergeVert2.out" "ana_n_hole_111Shape.i";
connectAttr "groupId1.id" "ana_n_hole_111Shape.iog.og[0].gid";
connectAttr "ana_n_hole_111SG.mwc" "ana_n_hole_111Shape.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "ana_n_hole_111SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "ana_n_hole_111SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "stage_uv_01_3.oc" "lambert3.c";
connectAttr "lambert3.oc" "ana_n_hole_111SG.ss";
connectAttr "ana_n_hole_111Shape.iog.og[0]" "ana_n_hole_111SG.dsm" -na;
connectAttr "groupId1.msg" "ana_n_hole_111SG.gn" -na;
connectAttr "ana_n_hole_111SG.msg" "materialInfo1.sg";
connectAttr "lambert3.msg" "materialInfo1.m";
connectAttr "stage_uv_01_3.msg" "materialInfo1.t" -na;
connectAttr "place2dTexture1.o" "stage_uv_01_3.uv";
connectAttr "place2dTexture1.ofu" "stage_uv_01_3.ofu";
connectAttr "place2dTexture1.ofv" "stage_uv_01_3.ofv";
connectAttr "place2dTexture1.rf" "stage_uv_01_3.rf";
connectAttr "place2dTexture1.reu" "stage_uv_01_3.reu";
connectAttr "place2dTexture1.rev" "stage_uv_01_3.rev";
connectAttr "place2dTexture1.vt1" "stage_uv_01_3.vt1";
connectAttr "place2dTexture1.vt2" "stage_uv_01_3.vt2";
connectAttr "place2dTexture1.vt3" "stage_uv_01_3.vt3";
connectAttr "place2dTexture1.vc1" "stage_uv_01_3.vc1";
connectAttr "place2dTexture1.ofs" "stage_uv_01_3.fs";
connectAttr ":defaultColorMgtGlobals.cme" "stage_uv_01_3.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "stage_uv_01_3.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "stage_uv_01_3.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "stage_uv_01_3.ws";
connectAttr "groupParts1.og" "polyCut1.ip";
connectAttr "ana_n_hole_111Shape.wm" "polyCut1.mp";
connectAttr "polySurfaceShape1.o" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polyCut1.out" "polyCut2.ip";
connectAttr "ana_n_hole_111Shape.wm" "polyCut2.mp";
connectAttr "polyCut2.out" "polyMergeVert1.ip";
connectAttr "ana_n_hole_111Shape.wm" "polyMergeVert1.mp";
connectAttr "polyMergeVert1.out" "polySplit1.ip";
connectAttr "polySplit1.out" "polySplit2.ip";
connectAttr "polySplit2.out" "polySplit3.ip";
connectAttr "polySplit3.out" "polyDelEdge1.ip";
connectAttr "polyDelEdge1.out" "polySplit4.ip";
connectAttr "polySplit4.out" "polyMergeVert2.ip";
connectAttr "ana_n_hole_111Shape.wm" "polyMergeVert2.mp";
connectAttr "ana_n_hole_111SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "stage_uv_01_3.msg" ":defaultTextureList1.tx" -na;
// End of ana_n_hole_111.ma
